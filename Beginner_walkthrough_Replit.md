# Beginner Walkthrough for using the Looker API with Python on Replit

If you weren't able to install python on your machine, don't worry! You can still try out the Looker API

You can use a virtual computer on a site called Replit to test out your code. 

https://replit.com/@FredHintz/LookerAPISample

First, create a fork of the sample replit that we've created for you by clicking the "Fork" button 

![Fork Button](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/ForkButton.png)

You will need to create an account on replit to be able to do this. Use an email address you have access to in order to create the account. 

Now you should see a copy of our code on your screen. Replit is an online Integrated Development Environment that allows you to run code on one of their computers. The free tier only gives you a small amount of processing power and RAM, but for our purposes that's totally fine. 

On the left hand side you'll see a File tree with the files that are in the replit. You should be on the file "main.py". 

![Replit IDE Layout](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/Replit_IDE_layout.png)

Before you can run the code, you need to make sure that it's configured to connect to your Looker account. 

You can save your credentials using Replit Secrets. The Replit secrets button is on the bottom left hand side of the page. It opens a window on the right pane. 

![Replit Secrets Button](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/Replit_secrets_button.png)

Replit secrets are encrypted are not shared in your public repl project. They are only accessible to your user. It's a safe way to store credentials. On your own machine you would store them in a ".ini" file. 

You'll need to create three secrets and name them "LOOKERSDK_BASE_URL", "LOOKERDK_CLIENT_ID", and "LOOKERSDK_CLIENT_SECRET". 

"LOOKERSDK_BASE_URL" will need to have the value "https://looker.clarityhs.com:19999/", or "https://looker-b.clarityhs.com:19999/", depending on which looker instance your community uses. 

Create a secret called "LOOKERSDK_CLIENT_ID" and enter your looker API Client ID into the "Secret" value. 

Create a secret called "LOOKERSDK_CLIENT_SECRET" and enter your looker API Client Secret into the "Secret" value. 

<b> DO NOT SAVE YOUR LOOKER API CLIENT OR YOUR LOOKER API SECRET IN ANY FILE ON REPLIT. </b> 

![Replit Add Secret](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/Replit_add_secret.png)

Now you should be able to run the sample code. 

## Run Inline Query in Looker API
One of the most useful features of the Looker API is the `run_inline_query()` endpoint. It allows the user to create queries, similar to an "Explore" in the Looker user interface. 

Looker queries are the "building" blocks of each visualization you can build in looker. They are what you use to get the data from the Clarity database in a usable format. 

Let's take a look at the file `Run_Inline_Query.py` in the Replit file explorer: 

Let's take a look at the `sdk.run_inline_query()` command that creates and runs a query on looker: 

```python
result = sdk.run_inline_query(
    result_format="csv", 
    body = {
        # Define your query
        "model":
        #Change the model to your community's site
        "data_sandbox_connection_model",
        "view":
        #This will query the HMIS Performance model. Change it if you want a different model.
        "base",
        "fields":
        # Change this list to query different fields.
        ["clients.id", "static_demographics.veteran_text", "clients.added_date"],
        "filters":
        # Use Looker Filter expressions to add filters to your explores
        {
            "clients.added_date": "after 2020-01-01"
        },
        # Tell Looker How many rows to return.
        "limit":
        100
            }
        )
```
There's a lot to configure with this command. 
There are a few important bits of information which are needed to build queries successfully.
Let's go through them one by one. 

```python}
    "model": "demo_connection_model",
```
The first bit of information is the `model`. `model` refers to your community's Clarity instance, with the word "connection_model" after it. If your community site is named demo.clarityhs.com, the "model" will be "demo_connection_model". 
Change the text in the `model` parameter to the model for your community. 


```python}
    "view": "base"                         
```
`view` refers to the "Explore" name used to create the query. In the documentation on the Clarity Web Site, we typically refer to these as "Models" (E.g., The HMIS Performance Model, the Client Model). Looker calls them "Explores". 

In the table below you can see what the Explore name is for each of the models you're used to seeing in Looker. For example, the explore "base" refers to the "HMIS Performance" Model. 

| Explore Name         | Model Name           |
| -------------------- | -------------------- |
| base                 | HMIS Performance     |
| client               | Coordinated Entry    |
| clients              | Services             |
| client_model         | Client               |
| data_quality         | Data Quality         |
| reservations_by_date | Reservations         |
| population           | Population Over Time |

For the sake of the example, let's leave this one the way it is. 

```python
    "fields": [
        "clients.id",
        "static_demographics.veteran_text",
        "clients.added_date"
    ]
```

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/fields.png)

The field name section is a list of all the field names that you want to return in your explore.  In Python, writing the fields out as a list means that each field name is in quotes and is separated by commas. You need to write out the field's full LookML name.  You can find the field's LookML name from the info bubble next to a dimension or measure name in the explore you're looking at. 

E.g, for the "Enrollment ID" it is `enrollments.id`.

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/looker-get-field-name.png)

The filters section of the query command does the same thing as the filters section in the Explore menu. 

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/Filters_look.png)

For this filter in the picture above, the Lookml name of the field "Clients: Date Created Date" is `clients.added_date`. 

In the code, we write out the filter as a [looker filter expression](https://cloud.google.com/looker/docs/filter-expressions). For this filter, we write that as `after 2020-10-01`. 

To make the filter statement we write the lookml field name in quotes, add a `:`, and then write the lookml filter expression. We then put the whole thing inside curly brackets, like so: `{"clients.added_date": "after 2020-10-01"}` 

If we have more than one filter, we separate them by a comma inside the curly brackets: 
``` 
    {"client.added_date": "after 2020-10-01",
     "agencies_creating_profile.name": "System"}
```

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/limit.png)

Limit corresponds to the "Row Limit" box in the looker explore options. It tells looker how many rows to return. If the query returns more than 500 rows, it will only show the first 500. 

The above definition will produce a query similar to an Explore seen in the image below. 

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/run-inline-query-like-explore.png)

Copy and paste the code in the `Run_Inline_Query.py` file into the `main.py` file under the section that says "Copy and Paste Code from the other Looker API Samples Here". 

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/Replit_where_to_paste.png)

Because of the settings in replit, it will only run the file called 'main.py' in the folder structure. Click the "Run" Button, and you should see the output from your query in the terminal!
![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/Replit_run_button.png)

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/Replit_inline_query_results.png)

### Running a Look 

Inline queries are perhaps the best way to ensure that you get consistent results from your API calls in looker. 

But what if you like using the User interface to create a query? Or what if you want to make sure that your API call is returning the same data as a look that you've created?

You can use the `run_look()` endpoint to do pull the results of a look that you've saved in the looker interface. 

You'll need the ID of the look that you want to get the results from. You can find the look's ID by examining the URL of the webpage your look is on. Open up the look and go to the address bar. Copy the number that appears after `looks/`. That is the Look ID you'll use to retrieve your results.

Copy and paste the code below into the `main.py` file in replit. 

```
#############
# Parameters
#############

#Switch this to a look ID that you've created
LOOK_ID = 21270
RESULT_FORMAT = "csv"

#############
# Pull Data
#############

# Pull results from Look.
result = sdk.run_look(LOOK_ID, RESULT_FORMAT)

print(result)
```

Check out the other examples in the replit repository: `Create_and_run_dashboards.py`, `Create_custom_fields.py`, `Create_schedules.py`

Look out for comments that say "Switch this" or "Change this here" to adapt the script to your instance. 
