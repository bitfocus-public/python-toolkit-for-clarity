
# Extensive
Python has become the ubiquitous tool for data science. Given it is a free and open source, thousands of companies from tech giants like Google and Facebook to individual niche users have adopted Python. 

* 

# Versatile

## Pull data from a database.
```python
# Read from DB into dataframe.
sql = """ 
         SELECT id,
                ST_AsGeoJSON(polygon,20)  
           FROM encampments
"""

df = pd.read_sql(sql_query, con=db_connection)
df.columns = ["id", "geometry"]

gdf = geopandas.GeoDataFrame(df)

#############
# Transform
#############
gdf["geometry"] = df["geometry"].apply(json.loads)
gdf["geometry"] = df["geometry"].apply(shape)

#############
# Load
#############
tp.Topology(gdf, prequantize=True).to_json()
```

# Readable

When developing a solution to problems, humans can rarely hold more than 4-6 concepts in their heads a once. This is often referred to as working memory. Unfortunately, SQL is an extremely challenging language when it comes to working memory. A query writer must often understand how 10-20 concepts interact in a single query.

Python, on the other hand, is a procedural language. Meaning, each bit of logic assessed a piece at a time. This allows the working memory of the query writer to better understand the entire process, as they only need to understand the inputs and logic of each step.



