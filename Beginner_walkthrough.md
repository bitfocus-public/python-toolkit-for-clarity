# Beginner Walkthrough for using the Looker API with Python

## Installing Anaconda
Anaconda is a distribution of python that contains a lot of other software that makes it easy to work with data. 

We recommend using it because it's completely free. 

* [Anaconda Installation Instructions for Windows](https://docs.anaconda.com/anaconda/install/windows/)

* [Anaconda Installation Instructions for Mac](https://docs.anaconda.com/anaconda/install/mac-os)

Or to jump right to download:
* [Download Anaconda](https://www.anaconda.com/products/individual#windows)

Download the installer and double-click on it.

![install-anaconda](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/anaconda-installer.PNG)

Leave all settings default and install. The installer will take a few minutes to complete, but should report "Installation Complete" when finished.

![install-anaconda](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/anaconda-installer-5.PNG)

## Opening Anaconda
After Anaconda is installed open the user interface by going to the Start Menu and selecting:

![install-anaconda](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/anaconda-navigator-open.PNG)

If all goes well, Anaconda should load. We'll come back to Anaconda in a moment. We still have a few more things we need to set up before we can go on. 


## Looker API

Looker is the business-intelligence platform Clarity uses for ad-hoc reporting. It is powerful and flexible. One of the powerful features is the API.

"API" stands for application programming interface. An API refers to a technological system's ability to be interacted with in a standardized way by another system. Or, you can think of it as a way to automate anything you'd do in the user interface. Retrieve data, create Looks, download CSVs, create schedules, etc.

Currently, credentials for the API may be requested through the Clarity Help Desk. A user must have a standalone Looker account. And their API usage must be comparable to an average user interacting with the user interface. These conditions and terms are subject to change without notice.

## Use the Looker API in Python
The hardest part of using the Looker API in Python is getting everything set up. But no worries, this article will walk you through it.

Before we start writing queries, we are going to install Looker's Python tools for interacting with Looker API. This takes some extra work out of creating Looker queries.

Windows: 
To install the Looker SDK, open the command prompt window by searching "Command Prompt" in the start menu or pressinb
```bash}
pip install looker-sdk
```

Mac: 
In your Applications folder, open "Terminal". Enter the following command into the terminal. 
```bash} 
pip install looker-sdk
```

This will install the Looker SDK into your Python environment, making it accessible from Spyder.

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/anaconda-navigator-install-looker-sdk-2.PNG)

You can find information about this Looker-supported Python SDK at:
* [PyPI looker_sdk](https://pypi.org/project/looker-sdk/)

## Setup Looker API Credentials
Before using the Looker Python SDK we must setup a file which will contain your API credentials. It is imperative you do not store these credentials as text in one of your scripts. And the `looker.ini` file we are about to create is meant to help you get started quickly, for more information on creating a more secure method of credential management see:

* [Securing your SDK credentials](https://github.com/looker-open-source/sdk-codegen/blob/main/README.md#securing-your-sdk-credentials)

**Your API credentials are essentially your username and password. Be extremely careful where you store them.**

If you do not have Looker API credentials, they may be requested through submitting a Clarity Help Desk ticket.

Warnings aside, let's create the `looker.ini` file.

Open Notepad 
![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/open-notepad.PNG)

And enter the following text replacing `LOOKER_API_ID` and `LOOKER_API_SECRET` with the credentials that were sent to you by the Clarity Help Desk. 

```bash
[Looker]
base_url=https://looker.clarityhs.com:19999

# API 4 client id
client_id=LOOKER_API_ID

# API 4 client secret
client_secret=LOOKER_API_SECRET

verify_ssl=True
```

In our example, we have you store your credentials on your user home directory. This is a good option because it's only accessible to your user and is generally hidden from someone browsing through the graphical interface. If you use a password manager like 1password, bitwarden, or others it's a good idea to save it there and then delete your .ini file after you use it today.  

## Introduction to Spyder

Now everything should be installed and configured. It's time to run some code. 

Let's talk a little bit about what Spyder is. Spyder is a program that allows for your code to be executed line-by-line and to view data that you've run and saved in the environment. 


## Running Code in Spyder

Let's test out Spyder. We're going to give you an example of storing data in what's called a data frame. It's a way that your data is going to be stored in your memory. It's really similar to a spreadsheet, like Microsoft Excel. It stores data in rows and columns. 

Copy the following code snippet into the Python code area (on the left). 

```python
import pandas as pd

data = {
        "col_1": [3, 2, 1, 0], 
        "col_2": ["a", "b", "c", "d"]
}

df = pd.DataFrame.from_dict(data)

print(df)
```

Once you've copied that code, click the green arrow button on the toolbar to run the script. This will send all of the code directly to the Python interpreter (bottom-right) and execute it. When the prompt returns, the code has been finished running.

Notice after executing the code the Variable Explorer (top-right) has a new entry called `df` of type `Dataframe`. Remember that word from before? If you click on it you'll notice it looks like a table. 

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/spyder-ide-code-executer.PNG)

Double click the "Value" associated with the variable named "df."
![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/spyder-ide-dataframe-viewer.PNG)

This dataframe consists of two columns, "col_1" and "col_2."  It also has an "Index," which will allow you to refer to the row number. 

When reading about dataframes you will see them associated with `pandas` a lot--`pandas` is a python library that makes working with data a lot easier. It allows for loading, transforming, and writing data from and to about any source or destination.

There is a lot to know about data science with python, but for our purposes, just knowing that a dataframe consists of a rows and columns with data in them is enough for now. 

If you're interested in more you can check out some of these resources: 

* [Pandas Cheat Sheet](https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf)
* [Python for Data Science and Machine Learning Bootcamp](https://www.udemy.com/course/python-for-data-science-and-machine-learning-bootcamp/) (expensive, but often on sale for $10-15).
* [DataCamp Data Analysis with Python Courses](https://www.datacamp.com/tracks/data-analyst-with-python)


## Testing the Looker API
Back in Spyder add the following script. Be sure to replace `"C:/Users/cthom/looker.ini` with the path to _your_ `looker.ini` file.

```python}
# Load the Looker Python SDK tools.
import looker_sdk

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/Users/cthom/looker.ini" # <--- Change 

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

# Retrieve my user information from Looker.
my_user = sdk.me()

# Display my username.
my_user["display_name"]
```

If everything works correctly, then you will see your username displayed in the Python console (bottom-right).

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/looker-api-test-result.PNG)

Let's go over what each line of the code is doing. You don't need to run this. 

```python
# Load the Looker Python SDK tools.
import looker_sdk
```
This line imports the package `looker_sdk`. It has some special commands that allow python to work with the Looker API that wouldn't be available to us if we didn't load this package. 

```python
# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/Users/cthom/looker.ini" # <--- Change 
```
This line sets the variable `CREDENTIALS_PATH` to the value "C:/Users/cthom/looker.ini". Essentially this is making it so that we don't have to type out the entire file path when we want to refer to it later. We can just type `CREDENTIALS_PATH` and Python will know what we mean. 

```python
# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)
```
This line initializes the looker API connection. There are a couple of things that we use from the two previous lines here. The command `init40` is a command that's part of the `looker_sdk` package. 

We tell python we want to use the `init40` command from the `looker_sdk` package (not some other `init40` command) by writing `looker_sdk.init40()`. 

The `init40` command sets up a connection between your computer and the looker server. It sends a message to the looker server that has your credentials and gets back a message that says that you connected OK. 

To tell it where your credentials are, we use the `CREDENTIALS_PATH` variable that we created. It's basically saying "use that location I told you about earlier that we called CREDENTIALS_PATH". 

The end result of this line is that we have an object in memory called `sdk` that is connected to our Looker application on the web. We can use that object to ask looker to do things for us or get information from looker. 

```python
# Retrieve my user information from Looker.
my_user = sdk.me()
```

This command uses the me command from the sdk that we just created. You can read about that endpoint on the [API explorer](https://developers.looker.com/api/explorer/4.0/methods/User/me?sdk=py)


```python
# Display my username.
my_user["display_name"]
```


## Endpoints

API Endpoints are like selections on a menu. Each endpoint asks the application to perform a different command. 

In our test, we used the "me" endpoint to ask looker to show us our user information. 

We didn't have to give it any instructions, because that endpoint is programmed to always return the information based on your user credentials. 

You can learn this information about any endpoint by reviewing Looker's detailed [API Documentation](https://developers.looker.com/api/explorer/4.0/methods?sdk=py)


## Run Inline Query in Looker API
One of the most useful features of the Looker API is the `run_inline_query()` endpoint. It allows the user to create queries, similar to an "Explore" in the Looker user interface. 

Looker queries are the "building" blocks of each visualization you can build in looker. They are what you use to get the data from the Clarity database in a usable format. 

These queries you define in code. E.g.,

A few important bits of information which are needed to build queries successfully.
```python}
    "model": "demo_connection_model",
```
`model` refers to your community's Clarity instance, with the word "connection_model" after it. If your community site is named demo.clarityhs.com, the "model" will be "demo_connection_model". 

```python}
    "view": "base"                         
```
`view` refers to the "Explore" name used to create the query. We typically refer to these as "Models" (E.g., The HMIS Performance Model, the Client Model). Looker calls them "Explores". 

| Explore Name         | Model Name           |
| -------------------- | -------------------- |
| base                 | HMIS Performance     |
| client               | Coordinated Entry    |
| clients              | Services             |
| client_model         | Client         |
| data_quality         | Data Quality         |
| reservations_by_date | Reservations         |
| population           | Population Over Time |

```python
    "fields": [
        "clients.id",
        "static_demographics.veteran_text",
        "clients.added_date"
    ]
```

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/fields.png)

The field name section is a list of all the field names that you want to return in your explore. You need to write out the field's full LookML name.  You can find the field's LookML name from the info bubble next to a dimension or measure name in the explore you're looking at. 

E.g, for the "Enrollment ID" it is `enrollments.id`. In Python, writing the fields out as a list means that each field name is in quotes and is separated by commas. 

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/looker-get-field-name.png)

The filters section of the query command does the same thing as the filters section in the Explore menu. 

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/Filters_look.png)

For this filter in the picture above, the Lookml name of the field "Clients: Date Created Date" is `clients.added_date`. 

In the code, we write out the filter as a [looker filter expression](https://cloud.google.com/looker/docs/filter-expressions). For this filter, we write that as `after 2020-10-01`. 

To make the filter statement we write the lookml field name in quotes, add a `:`, and then write the lookml filter expression. We then put the whole thing inside curly brackets, like so: `{"clients.added_date": "after 2020-10-01"}` 

If we have more than one filter, we separate them by a comma inside the curly brackets: 
``` 
    {"client.added_date": "after 2020-10-01",
     "agencies_creating_profile.name": "System"}
```

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/limit.png)

Limit corresponds to the "Row Limit" box in the looker explore options. It tells looker how many rows to return. If the query returns more than 500 rows, it will only show the first 500. 

```python}
    "model": "demo_connection_model",
    "view": "base",                         
    "fields": [
        "clients.id",
        "static_demographics.veteran_text",
        "clients.added_date"
    ],
    "filters": {"clients.added_date": "after 2020-01-01"},
    "limit": 500
```
The above definition will produce a query similar to an Explore seen in the image below. 

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/run-inline-query-like-explore.png)

Let's put it all together now. Copy and paste the code below into your Spyder window and run it. Make sure to change the location of your .ini file in the code to what you saved before. 

```python
#############
# Setup
#############

# Load the Looker Python SDK tools.
import looker_sdk
from io import StringIO
import pandas as pd

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/path/to/your/looker.ini" # <--- Change 

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

#############
# Parameters
#############
RESULT_FORMAT = "csv"

#############
# Send Query
#############

# Define your query
body = {
    "model": "demo_connection_model",
    "view": "base",                         
    "fields": [
        "clients.id",
        "static_demographics.veteran_text",
        "clients.added_date"
    ],
    "filters": {"clients.added_date": "after 2020-01-01"},
    "limit": 100
}

################
# Get Response
################

# Run the inline query
result = sdk.run_inline_query(
    result_format=RESULT_FORMAT, 
    body=body
)

# Convert to dataframe.
df = pd.read_csv(StringIO(result))

print(df)

```

If all goes well, your query will run and your results will be loaded into a dataframe.

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/looker-results-of-line-query.png)

Check out some other code examples on the public repository to do things like run a look, copy a dashboard, create a schedule, or create a custom dimension: https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/tree/master/scripts?ref_type=heads


```