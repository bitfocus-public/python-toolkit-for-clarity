"""
Geospatial examplesin Python:
    https://mattijn.github.io/topojson/example/input-types.html
"""

import pandas as pd
import json
import looker_sdk
import topojson as tp
import geopandas
from shapely.geometry import shape
import configparser
from io import StringIO

#############
# Setup
#############

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "<PATH_TO_LOOKER_CREDENTIALS>"  # <--- Change

config = configparser.ConfigParser()
config.read(CREDENTIALS_PATH)

#############
# Parameters
#############
BASE_URL = config["Looker"]["base_url"] + "/api/4.0"
RESULT_FORMAT = "csv"


#############
# Parameters
#############
CONNECTION = "<YOUR_CONNECTION_NAME>"
EXPLORE = "outreach"

#############
# Extract
#############

# Define your query
body = {
    "model": CONNECTION,
    "view": EXPLORE,                         
    "fields": [
        "outreach_geo_fencings.id",
        "outreach_geo_fencings.multi_polygon",
    ],
    "filters": {"outreach_geo_fencings.multi_polygon": "-NULL"},
    "limit": 100
}

################
# Get Response
################
# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

# Run the inline query
result = sdk.run_inline_query(
    result_format=RESULT_FORMAT, 
    body=body
)

df = pd.read_csv(StringIO(result))

df.columns = ["id", "geometry"]




#############
# Transform
#############

result = {}

df["geo_processed"] = ""
for idx, row in df.iterrows():
    df["geo_processed"].iloc[idx] = shape(json.loads(row["geometry"]))

gdf = geopandas.GeoDataFrame(df, geometry="geo_processed")

# #############
# Load
#############
tp.Topology(gdf, prequantize=True).to_json()