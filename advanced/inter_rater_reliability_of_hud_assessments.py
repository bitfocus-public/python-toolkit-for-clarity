"""
1. Pull HUD Entry Assessment Health information.
2. Numerically encode the data.
3. Loop ->
   1. Get all HUD Entry Assessment Health for client.
   2. Shape assessments into rows and assessors as columns.
   3. Perform Fleiss' Kappa and record in dictionary
4. Convert results into dataframe.

Fleiss' Kappa:
    https://www.youtube.com/watch?v=ga-bamq7Qcs
    https://en.wikipedia.org/wiki/Fleiss%27_kappa
    https://statistics.laerd.com/spss-tutorials/fleiss-kappa-in-spss-statistics.php

statsmodels:
    !conda install -c conda-forge statsmodels -y
    
statsmodels.
    https://www.statsmodels.org/dev/generated/statsmodels.stats.inter_rater.aggregate_raters.html#statsmodels.stats.inter_rater.aggregate_raters
"""

#############
# Setup
#############

import pandas as pd
import numpy as np
from sklearn import preprocessing
from statsmodels.stats.inter_rater import aggregate_raters
from statsmodels.stats.inter_rater import fleiss_kappa

from tqdm import tqdm
import matplotlib.pyplot as plt

# Load the Looker Python SDK tools.
import looker_sdk
from io import StringIO

pd.options.mode.chained_assignment = None

############
# Parameters
############

CREDENTIALS_PATH = "<PATH_TO_LOOKER_CREDENTIALS>"  # <--- Change
RESULT_FORMAT = "csv"

MODEL = "<YOUR_CONNECTION_NAME>"
EXPLORE = "client_model"

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

# Calculation parameters
NUM_ASSESSMENTS_REQUIRE = 2
NUM_FREQ_BINS = 20


############
# Pull Data
############
fields = [
    "client_model.id",
    "enrollments.id",
    "entry_screen.ref_user",
    "entry_screen.disabling_condition",
    "entry_screen.health_dev_disability",
    "entry_screen.health_hiv",
    "entry_screen.health_mental",
    "entry_screen.health_phys_disability",
    "entry_screen.health_substance_abuse",
]

filters = {}

df = pd.read_csv(
    StringIO(
        sdk.run_inline_query(
            RESULT_FORMAT,
            looker_sdk.models31.WriteQuery(
                model=MODEL,
                view=EXPLORE,
                fields=fields,
                filters=filters,
                limit=-1,
                sorts="",
            ),
        )
    )
)


df.columns = fields

################
# Pull Data
################
dfs = df[
    [
        "client_model.id",
        "entry_screen.ref_user",
        "entry_screen.disabling_condition",
        "entry_screen.health_dev_disability",
        "entry_screen.health_hiv",
        "entry_screen.health_mental",
        "entry_screen.health_phys_disability",
        "entry_screen.health_substance_abuse",
    ]
]


################
# Encode
################

le = preprocessing.LabelEncoder()

categorical_columns = [
    "entry_screen.disabling_condition",
    "entry_screen.health_dev_disability",
    "entry_screen.health_hiv",
    "entry_screen.health_mental",
    "entry_screen.health_phys_disability",
    "entry_screen.health_substance_abuse",
]

# Ensure no empty columns.
dfs.fillna("na", inplace=True)

# Numerically encode each column's values.
tmp = pd.DataFrame(dfs)
for cat_column in categorical_columns:
    tmp[cat_column] = le.fit_transform(dfs[cat_column])

dfs = tmp
del tmp


# Clean up.
dfs = dfs.dropna()

#############
# Loop
#############

# Get a list of all personal_ids. This will allow us
# to loop through all clients and calculate Fleiss' Kappa
# on their Entry HUD Assessments.
personal_ids = dfs["client_model.id"].unique().tolist()

results = {}

for personal_id in tqdm(personal_ids):
    sub_df = dfs.loc[df["client_model.id"] == personal_id]

    # If a client only has 1 Enrollment, skip the calculation.
    if sub_df.shape[0] < NUM_ASSESSMENTS_REQUIRE:
        continue

    # Shape data into (row, col) = (enrollments, assessors)
    data = np.array(sub_df[categorical_columns]).transpose()

    ################################
    # Fleiss' Kappa per Client
    ################################

    ar = aggregate_raters(data=data)
    fk = fleiss_kappa(ar[0], method="fleiss")

    results[personal_id] = fk


# Convert the dictionary to a dataframe
fleiss_kappa_df = pd.DataFrame().from_dict(results, orient="index").reset_index()

# Label the columns.
fleiss_kappa_df.columns = ["personal_id", "fleiss_kappa"]

mean = fleiss_kappa_df["fleiss_kappa"].mean()

###############################################
# Plot Frequency Distribution of Kappa Scores
###############################################

plt.hist(
    fleiss_kappa_df["fleiss_kappa"], NUM_FREQ_BINS
)  # probabilities store a series of float numbers
plt.xlabel("Count of Kappas")
plt.ylabel("Frequency")
plt.show()
