"""
1. Setup Gmail "App Password": https://towardsdatascience.com/automate-sending-emails-with-gmail-in-python-449cc0c3c317
2. Pull data from a Look.
3. Create HTML embedded with data.
4. Send email.
"""

#############
# Setup
#############

# Load the Looker Python SDK tools.
import looker_sdk

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from datetime import datetime
from io import StringIO
import json
import pandas as pd

############
# Parameters
############

CREDENTIALS_PATH = "<PATH_TO_LOOKER_CREDENTIALS>"  # <--- Change
EMAIL_CREDENTIALS_PATH = "<JSON_FILE_WITH_EMAIL_CREDS>"
MODEL = "<YOUR_CONNECTION>"
EXPLORE = "data_quality"

# Load email password
with open(EMAIL_CREDENTIALS_PATH, "r") as f:
    FROM_PASS = json.load(f)["gmail_app"]


RESULT_FORMAT = "csv"
FROM = "<FROM_EMAIL>"
EMAIL_TEMPLATE_PATH = "./email_template.html"
EMAIL_TITLE = "Assessments Due"
SUBJECT = "Assessments Due"

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

#######################################
# Users with Annual Assessments Due
#######################################

fields = [
    "members.email",
    "members.first_name",
    "members.last_name",
    "dq_annual_assessments.targeted_annual_assessment",
    "dq_client_programs.id,data_quality.id",
    "dq_annual_assessments.targeted_annual_assessment",
    "dq_annual_assessments.annual_assessment_status"
]

filters = {
    "dq_client_programs.date_filter": "",
    "dq_annual_assessments.targeted_annual_assessment": "today for 60 days",
    "dq_annual_assessments.annual_assessment_status": "Due",
}

df = pd.read_csv(
    StringIO(
        sdk.run_inline_query(
            RESULT_FORMAT,
            looker_sdk.models31.WriteQuery(
                model=MODEL, view=EXPLORE, fields=fields, filters=filters, limit=-1
            ),
        )
    )
)

# The actual sending of the e-mail
server = smtplib.SMTP('smtp.gmail.com:587')
server.starttls()
server.login(FROM, FROM_PASS)

# Gather staff info for looping.
staff_info = df[["Staff Email", "Staff First Name", "Staff Last Name"]].drop_duplicates()
staff_records = staff_info.to_dict(orient="records")

# Loop through all users in the Data Quality query.
for staff_record in staff_records:
    
    #######################
    # Prepare Message
    #######################
     
    first_name = staff_record["Staff First Name"]
    last_name = staff_record["Staff Last Name"]

    name = f"{first_name} {last_name}"
        
    all_due = df[df["Staff Email"] == staff_record["Staff Email"]]
    all_due = df[["Enrollments Enrollment ID", "Clients Personal ID", "DQ Annual Assessments Targeted Annual Assessment"]]    
    
    content = f"Hey {name}, could you please update these assessments at your earliest convenience."
    body = f"""{content}"""
    
    with open(EMAIL_TEMPLATE_PATH, "r") as f:
        email_template = (
            f.read()
            .replace("{{title}}", EMAIL_TITLE)
            .replace("{{body}}", body)
            .replace("{{table}}", all_due.to_html())
            .replace("{{date}}", datetime.now().strftime("%Y-%m-%d"))
        )
    
    #############
    # Send Email
    #############
    
    # Replace with the following to send to users:
    TO = row["Staff Email"]
    
    content = [email_template]
    
    MESSAGE = MIMEMultipart('alternative')
    MESSAGE['subject'] = SUBJECT
    MESSAGE['To'] = TO
    MESSAGE['From'] = FROM
    MESSAGE.preamble = """
    Your mail reader does not support the report format.
    Please visit us online!"""
    
    # Record the MIME type text/html.
    HTML_BODY = MIMEText(email_template, 'html')
    
    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    MESSAGE.attach(HTML_BODY)

    # Send the email.
    server.sendmail(FROM, [TO], MESSAGE.as_string())
        
# Be sure and quit email server.
server.quit()