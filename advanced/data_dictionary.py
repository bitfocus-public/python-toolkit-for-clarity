#############
# Setup
#############

# Load the Looker Python SDK tools.
import requests
import pandas as pd
import configparser

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "<PATH_TO_LOOKER_CREDENTIALS>"  # <--- Change

config = configparser.ConfigParser()
config.read(CREDENTIALS_PATH)

#############
# Parameters
#############
BASE_URL = config["Looker"]["base_url"] + "/api/3.1"
RESULT_FORMAT = "csv"

CONNECTION_MODEL = "<YOUR_CONNECTION_NAME>"
EXPLORE = "data_quality"

OUTPUT_DIR = "<OUTPUT_PATH>"


#############
# Login to Looker
#############

payload = {
    "client_id": config["Looker"]["client_id"],
    "client_secret": config["Looker"]["client_secret"],
}
url = BASE_URL + "/login"
r = requests.post(url, data=payload)
token = r.json()["access_token"]

#############
# Pull LookML
#############
headers = {
    "Authorization": "token " + token,
    "Content-Type": "application/json",
}
payload = {
    "lookml_model_name": CONNECTION_MODEL,
    "explore_name": EXPLORE,
}
url = BASE_URL + "/lookml_models/" + CONNECTION_MODEL + "/explores/" + EXPLORE

result = dict(requests.get(url, params=payload, headers=headers).json())

##################################
# Create a HTML Data Dictionary
##################################


html_style = """<style>
.styled-table {
    border-collapse: collapse;
    margin: 25px 0;
    font-size: 0.9em;
    font-family: sans-serif;
    min-width: 400px;
    box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
}

.styled-table thead tr {
    background-color: #009879;
    color: #ffffff;
    text-align: left;
}

.styled-table th,
.styled-table td {
    padding: 12px 15px;
}

.styled-table tbody tr {
    border-bottom: 1px solid #dddddd;
}

.styled-table tbody tr:nth-of-type(even) {
    background-color: #f3f3f3;
}

.styled-table tbody tr:last-of-type {
    border-bottom: 2px solid #009879;
}

.styled-table tbody tr.active-row {
    font-weight: bold;
    color: #009879;
}

</style>
"""


def save_fields_to_html_table(result, name):
    fields = result["fields"][name]
    fields = pd.DataFrame(fields)

    html = (
        html_style
        + "\n"
        + fields.to_html().replace("<table ", "<table class='styled-table'")
    )

    with open(OUTPUT_DIR + f"{name}.html", "+w") as f:
        f.write(html)


save_fields_to_html_table(result, "dimensions")
save_fields_to_html_table(result, "measures")
save_fields_to_html_table(result, "filters")
save_fields_to_html_table(result, "parameters")
