#############
# Setup
#############

# Load the Looker Python SDK tools.
import looker_sdk
from io import StringIO
import pandas as pd
from looker_sdk.sdk.api40 import models as models

import json

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/path/to/your/looker.ini"  # <--- Replace

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

#############
# Parameters
#############
RESULT_FORMAT = "csv"

#############
# Send Query
#############

# Define your query
body = models.WriteQuery(
    model="demo_connection_model",
    view="base",
    fields=[
        "clients.id",
        "static_demographics.veteran_text",
        "clients.added_date",
        "over_18",
    ],
    filters={"clients.added_date": "after 2020-01-01"},
    limit="500",
    dynamic_fields=json.dumps(
        [
            {
                "category": "dimension",
                "description": "Is the individual over the age of 18?",
                "expression": 'if(${entry_screen.age} > 18, "Over 18", "Under 18")',
                "label": "Over 18",
                "value_format": None,
                "value_format_name": None,
                "dimension": "over_18",
            }
        ]
    ),
)

################
# Get Response
################

# Run the inline query
result = sdk.run_inline_query(result_format=RESULT_FORMAT, body=body)

# Convert to dataframe.
df = pd.read_csv(StringIO(result))
