###############
# Import Tools
###############
import configparser
from sqlalchemy import create_engine
import pandas as pd

###############
# Load DB Info.
###############
config = configparser.ConfigParser()
config.read("C:/Users/cthom/clarity_db.ini")  # <-- Change

###############
# Connect to DB
###############
db_connection = create_engine(
    "mysql+pymysql://"
    + config["DEFAULT"]["DB_USERNAME"]
    + ":"
    + config["DEFAULT"]["DB_PASSWORD"]
    + "@"
    + config["DEFAULT"]["DB_HOST"]
    + "/"
    + config["DEFAULT"]["DB_NAME"]
)


##############################
# Read clients into Dataframe
##############################
sql_query = """
    SELECT c.id             AS personal_id,
           cp.id            AS enrollment_id,
           cp.start_date    AS start_date,
           cp.end_date      AS end_date,
           p.ref_category   AS project_type
      FROM clients AS c
      JOIN client_programs AS cp ON c.id = cp.ref_client
      JOIN programs AS p ON cp.ref_program = p.id
     WHERE cp.start_date > DATE_SUB(NOW(), INTERVAL 31 DAY)
"""

# Read from DB into dataframe.
df = pd.read_sql(sql_query, con=db_connection)
