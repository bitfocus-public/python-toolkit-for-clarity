#############
# Setup
#############
# Load the Looker Python SDK tools.
import looker_sdk

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/path/to/your/looker.ini"  # <--- Replace

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

#############
# Parameters
#############
LOOK_ID = "53455"  # <--- Replace with your Look ID.
RESULT_FORMAT = "csv"

#############
# Pull Data
#############

# Pull results from Look.
result = sdk.run_look(LOOK_ID, RESULT_FORMAT)
