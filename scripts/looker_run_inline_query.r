
# Install devtools
install.packages("devtools")
install.packages("httpuv")

# Install lookr
devtools::install_github("looker/lookr")

# Load lookr library.
library(lookr)

# Connect to Looker API.
sdk <- LookerSDK$new(configFile = "PATH_TO_YOUR_LOOKER_INI")

# Query a modle
CONNECTION <- "la_connection_model"

# Explore name, for 
VIEW = "base"

FIELDS <- c(
  "clients.id",
  "static_demographics.veteran_text",
  "clients.added_date"
)

results <- sdk$runInlineQuery(
  model = CONNECTION,
  view = VIEW,
  fields = FIELDS,
  limit = 500,
  queryTimezone = "America/Los_Angeles"
)

# Convert list of list to dataframe.
df <- as.data.frame(do.call(rbind, results))