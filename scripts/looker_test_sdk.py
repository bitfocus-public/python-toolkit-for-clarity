# Load the Looker Python SDK tools.
import looker_sdk

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/path/to/your/looker.ini"  # <--- Replace

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

# Retrieve my user information from Looker.
my_user = sdk.me()

# Display my username.
my_user["display_name"]
