#############
# Setup
#############
# Load the Looker Python SDK tools.
import looker_sdk
from looker_sdk.sdk.api40 import models as models
from time import sleep

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/path/to/your/looker.ini"  # <--- Change

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

#############
# Parameters
#############

# Where to save PDF.
PATH_TO_SAVE_OUTPUT = "C:/path/to/your/Desktop/Amazing_Dashboard.pdf"

# How many minutes to if render completed before timing out.
TIMEOUT_MINS = 15

# Dashboard ID. Found in the URL of the dashboard.
DASHBOARD_ID = "498"

# Output type: pdf, png, or jpg
RESULT_FORMAT = "pdf"

BODY = models.CreateDashboardRenderTask(
    dashboard_style="tiled",
    # ADD FILTERS HERE
    # E.g.,
    # "dashboard_filters": {
    #   "Created Date=12 months ago for 12 months"
    # }
)

# Output width in pixels
WIDTH = 640

# Output height in pixels
HEIGHT = 480

# Paper size for pdf. Value can be one of: ["letter","legal","tabloid","a0","a1","a2","a3","a4","a5"]
PDF_PAPER_SIZE = "letter"

# Whether to render pdf in landscape paper orientation
PDF_LANDSCAPE = True

#############
# Pull Data
#############

# Pull results from Look.
result = sdk.create_dashboard_render_task(
    dashboard_id=DASHBOARD_ID,
    body=BODY,
    result_format=RESULT_FORMAT,
    width=WIDTH,
    height=HEIGHT,
    pdf_paper_size=PDF_PAPER_SIZE,
    pdf_landscape=PDF_LANDSCAPE,
)

task_id = result["id"]

#################################
# Wait for Dashboard to Render
#################################
# Below, we are going to check every minute
# if Looker has finished rendering your
# dashboard. When it has, it saves it to
# as a file.

for minute in range(TIMEOUT_MINS):

    # Check if the dashboard has been rendered.
    task = sdk.render_task(task_id)["status"]
    print(f"Render is: {task}")

    if task == "success":

        # Get rendered results.
        result = sdk.render_task_results(task_id)

        # Write PDF.
        with open(PATH_TO_SAVE_OUTPUT, "wb+") as f:
            print(f"Dashboard saved to {PATH_TO_SAVE_OUTPUT}")
            f.write(result)

        # Quit early, if done.
        break

    else:
        print("Still not done, we'll check again in a minute.")
        sleep(60)
