###################
# Setup
###################

# Load the Looker Python SDK tools.
import looker_sdk
from io import StringIO
import configparser
from sqlalchemy import create_engine
import pandas as pd

###################
# Parameters
###################
RESULT_FORMAT = "csv"


###################
# Load DB Info.
###################
config = configparser.ConfigParser()
config.read("C:/path/to/your/db_creds.ini")  # <-- Change

###################
# Load Looker Info
###################
# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/path/to/your/looker.ini"  # <--- Change

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

###############
# Connect to DB
###############
db_connection = create_engine(
    "mysql+pymysql://"
    + config["DEFAULT"]["DB_USERNAME"]
    + ":"
    + config["DEFAULT"]["DB_PASSWORD"]
    + "@"
    + config["DEFAULT"]["DB_HOST"]
    + "/"
    + config["DEFAULT"]["DB_NAME"]
)

##############################
# Query SQL database
##############################
sql_query = """
    SELECT c.id             AS personal_id,
           cp.id            AS enrollment_id,
           cp.start_date    AS start_date,
           cp.end_date      AS end_date,
           p.ref_category   AS project_type
      FROM clients AS c
      JOIN client_programs AS cp ON c.id = cp.ref_client
      JOIN programs AS p ON cp.ref_program = p.id
     WHERE cp.start_date > DATE_SUB(NOW(), INTERVAL 31 DAY)
"""

clarity_db_df = pd.read_sql(sql_query, con=db_connection)

##############################
# Get Chronic Homelesness
##############################

body = {
    "model": "demo_connection_model",
    "view": "base",
    "fields": ["enrollments.id", "chronic_homeless.ch_at_project_start"],
    "limit": -1,
}

looker_results = sdk.run_inline_query(result_format=RESULT_FORMAT, body=body)

chronic_homeless_df = pd.read_csv(StringIO(looker_results))

# Rename column names for ease of use.
chronic_homeless_df.columns = ["enrollment_id", "chronic"]

##############################
# Merge Results
##############################

# Join the two dataframes on enrollment_id
df = clarity_db_df.merge(chronic_homeless_df, how="left", on="enrollment_id")
