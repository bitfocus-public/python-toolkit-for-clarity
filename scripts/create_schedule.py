#############
# Setup
#############
# Load the Looker Python SDK tools.
import looker_sdk
from looker_sdk.sdk.api40 import models as models
from time import sleep

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/path/to/your/looker.ini"  # <--- Change

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

#############
# Parameters
#############
NAME_OF_SCHEDULE = "BNL Updates"
USER_ID_SENDING_SCHEDULE = "832"
RUN_AS_RECIPIENT = False  # Controls whether to run the query as the recipient.
SCHEDULE_FILTERS = ""

# See below for creating a cron string.
# https://crontab.guru/
CRON_STRING_FOR_SCHEDULE_FREQUENCY = "0 6 1 * *"
RECIPIENTS = ["someones-email@bitfocus.com", "demo@bitfocus.com"]


# Either the `look_id` or `dashboard_id` should be used. Not both.
LOOK_ID = "1670"
# DASHBOARD_ID = "498"


#############
# Setup
#############
schedule_plan_destination = [
    models.ScheduledPlanDestination(
        format="inline_table",
        apply_formatting=True,
        apply_vis=True,
        address=email,
        type="email",
    )
    for email in RECIPIENTS
]

###################
# Create Schedule
###################
body = models.WriteScheduledPlan(
    name=NAME_OF_SCHEDULE,
    user_id=USER_ID_SENDING_SCHEDULE,
    run_as_recipient=False,
    enabled=False,
    
    # Either the `look_id` or `dashboard_id` should be used. Not both.
    # dashboard_id= DASHBOARD_ID,
    look_id=LOOK_ID,
    
    # Uncomment to add schjedule filters.
    # filters_string=SCHEDULE_FILTERS,
    require_results=False,
    require_no_results=False,
    require_change=False,
    send_all_results=False,
    crontab=CRON_STRING_FOR_SCHEDULE_FREQUENCY,
    timezone="America/Los_Angeles",
    scheduled_plan_destination=schedule_plan_destination,
    include_links=True,
)

result = sdk.create_scheduled_plan(body)
