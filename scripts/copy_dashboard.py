"""
The following example shows how to copy a dashboard from one folder
to another. The home folder ID is not needed, as the Looker API finds the 
home folder based on the Dashboard ID. The destination folder can be changed
by setting the "DESTINATION_FOLDER_ID." This ID can be found in the URL
when visiting the folder in the UI.
"""
#############
# Setup
#############

# Load the Looker Python SDK tools.
import looker_sdk

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/path/to/your/looker.ini"  # <--- Replace

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

#############
# Parameters
#############
DASHBOARD_ID = "5472"  # <--- Replace with your Dashboard ID.
DESTINATION_FOLDER_ID = "18"  # <--- Replace with your Destination Folder ID.

#################
# Copy Dashboard
#################
result = sdk.copy_dashboard(DASHBOARD_ID, DESTINATION_FOLDER_ID)
