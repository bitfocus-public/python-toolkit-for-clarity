# Python Toolkit for Clarity

A collection of articles and code examples for using Python export and import data from Clarity.

## Installing Anaconda
Anaconda is a Python based data science package management system. Fancy terms for a collection of tools to make working with data much, _much_ easier.

Installing it is pretty easy.

* [Anaconda Installation Instructions for Windows](https://docs.anaconda.com/anaconda/install/windows/)

Or to jump right to download:
* [Download Anaconda](https://www.anaconda.com/products/individual#windows)

Download the installer and double-click on it.

![install-anaconda](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/anaconda-installer.PNG)

Leave all settings default and install. The installer will take a few minutes to complete, but should report "Installation Complete" when finished.

![install-anaconda](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/anaconda-installer-5.PNG)

## Opening Anaconda
After Anaconda is installed open the user interface by going to the Start Menu and selecting:

![install-anaconda](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/anaconda-navigator-open.PNG)

There are many tools in Anaconda, but the one we want to focus on will allow us to write Python data transformations and review results immediately.

Click the "Launch" button below "Spyder:
![install-anaconda](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/anaconda-navigator-panel.PNG)


## Introduction to Spyder
Spyder is a great data science integrated development environment (IDE). It allows for code to be executed line-by-line and the results explored in their "Variable Explorer."

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/spyder-ide-tour.PNG)

The "Variable Explore" is the best feature of Sypder. It saves an enormous amount of time when developing data transformations, as you do not need to save the data to file then open it in Excel or CSV viewer merely to inspect if the transformation was successful. In short, it allows for extremely fast development of data transformations.

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/spyder-ide-variable-viewer.PNG)

## Running Code in Spyder

Let's test out Spyder. Copy the following code snippet into the Python code area (on the left). 

```python
import pandas as pd

data = {
        "col_1": [3, 2, 1, 0], 
        "col_2": ["a", "b", "c", "d"]
}

df = pd.DataFrame.from_dict(data)
```

Once there, select all the text and press the `F9` key. This will send all of the code directly to the Python interpreter (bottom-right) and execute it. When the prompt returns, the code has been finished running.

Notice after executing the code the Variable Explorer (top-right) has a new entry called `df` of type `Dataframe`. In the Python world, when data stored in rows and columns, like those out of a CSV or database table, are stored in memory it is called a "dataframe." 

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/spyder-ide-code-executer.PNG)

Double click the "Value" associated with the variable named "df."
![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/spyder-ide-dataframe-viewer.PNG)

This dataframe consists of two columns, "col_1" and "col_2."  It also has an "Index," which will allow you to refer to the row number. 

When reading about dataframes you will see them associated with `pandas` a lot--`pandas` is the primary toolkit for a data scientist in Python. It allows for loading, transforming, and writing data from and to about any source or destination.

There is a lot to dataframes in the Python world, as this article is Clarity centric, I'll refer you to these getting started articles:

* [Pandas Cheat Sheet](https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf)
* [Python for Data Science and Machine Learning Bootcamp](https://www.udemy.com/course/python-for-data-science-and-machine-learning-bootcamp/) (expensive, but often on sale for $10-15).

## Looker API
When it comes to retrieving data from the Clarity system there are two primary methods [Clarity SQL Model](https://gitlab.com/bitfocus-public/clarity_custom_sql_model) and the Looker API.

Looker is the business-intelligence platform Clarity uses for ad-hoc reporting. It is powerful and flexible. One of the powerful features is the API.

A real quick clarification, "API" stands for application programming interface. An API refers to a technological system's ability to be interacted with in a standardized way by another system. Or, you can think of it as a way to automate anything you'd do in the user interface. Retrieve data, create Looks, download CSVs, create schedules, etc.

If all this seems too abstract, no worries. We are going to use real examples below to help solidify concepts.

One last note on the Clarity Looker API usage. Currently, credentials for the API may be requested through the Clarity Help Desk. A user must have a standalone Looker account. And their API usage must be comparable to an average user interacting with the user interface. These conditions and terms are subject to change without notice.

## Use the Looker API in Python
The hardest part of using the Looker API in Python is getting everything setup. But no worries, this article will walk you through it.

Before we start writing queries, we are going to install Looker's Python tools for interacting with Looker API. This takes some extra work out creating Looker queries.

To install these tools, open the Anaconda Navigator and open `CMD.exe Prompt`. Then type:
```bash
pip install looker-sdk
```

This will install the Looker SDK into your Python environment, making it accessible from Spyder.

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/anaconda-navigator-install-looker-sdk-2.PNG)

You can find information about this Looker supported Python SDK at:
* [PyPI looker_sdk](https://pypi.org/project/looker-sdk/)

## Setup Looker API Credentials
Before using the Looker Python SDK we must setup a file which will contain your API credentials. It is imperative you do not store these credentials as text in one of your scripts. And the `looker.ini` file we are about to create is meant to help you get started quickly, for more information on creating a more secure method of credential management see:

* [Securing your SDK credentials](https://github.com/looker-open-source/sdk-codegen/blob/main/README.md#securing-your-sdk-credentials)

**Your API credentials are essentially your username and password. Be extremely careful where you store them.**

If you do not have Looker API credentials, they may be requested through submitting a Clarity Help Desk ticket.

Warnings aside, let's create the `looker.ini` file.

Open Notepad 
![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/open-notepad.PNG)

And enter the following text replacing `LOOKER_API_ID` and `LOOKER_API_SECRET` with our respective credentials.

```bash
[Looker]
base_url=https://looker.clarityhs.com:19999

# API 4 client id
client_id=LOOKER_API_ID

# API 4 client secret
client_secret=LOOKER_API_SECRET

verify_ssl=True
```

## Testing the Looker API
Back in Spyder add the following script. Be sure to replace `"C:/Users/cthom/looker.ini` with the path to _your_ `looker.ini` file.

```python
# Load the Looker Python SDK tools.
import looker_sdk

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/Users/cthom/looker.ini" # <--- Change 

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

# Retrieve my user information from Looker.
my_user = sdk.me()

# Display my username.
my_user["display_name"]
```

If everything works correctly, then you will see your username displayed in the Python console (bottom-right).

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/looker-api-test-result.PNG)

## Retrieve results from Look 
One of the fundamental parts of using Looker is creating "[Looks](https://docs.looker.com/sharing-and-publishing/viewing-looks#making_sense_of_a_look)." These are queries which can be saved and added to dashboards, _and_ may be accessed through the API. This last feature is pretty cool--as it allows a report writer to pull data from existing content, without interfering with the user of the content.

Let's try it out. Add the code below to your Spyder script. Replace the `LOOK_ID` with whatever Look you would like to pull results from. And replace `RESULT_FORMAT` with how you would like the data returned. Here, we are are asking for CSV. 

```python
#############
# Setup
#############
# Load the Looker Python SDK tools.
import looker_sdk

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/Users/cthom/looker.ini" # <--- Change 

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

#############
# Parameters
#############
LOOK_ID = "21270"
RESULT_FORMAT = "csv"

#############
# Pull Data
#############

# Pull results from Look.
result = sdk.run_look(LOOK_ID, RESULT_FORMAT)
```

After you've modified the script, select all the text and press `F9` to run it. If all goes well you should have no errors in the outcome. And your "View Explorer" will now have a new variable. If you open it you should see the results as raw CSV text.

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/run-look-results.PNG)


## Convert CSV Results to Dataframe
Great! We've pulled some data, but now what? Having it as a raw text does not make it very useful. It would be better as a dataframe, where we can perform Python data transformations before writing it to a file.

Luckily, this transformation is extremely easy. Let's modify the script above to convert the results to a dataframe:

```python
#############
# Setup
#############
# Load the Looker Python SDK tools.
import looker_sdk
from io import StringIO
import pandas as pd

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/Users/cthom/looker.ini" # <--- Change 

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

#############
# Parameters
#############
LOOK_ID = "21270"
RESULT_FORMAT = "csv"

#############
# Pull Data
#############

# Pull results from Look.
result = sdk.run_look(LOOK_ID, RESULT_FORMAT)
# Convert the CSV results to a Pandas dataframe.
df = pd.read_csv(StringIO(result))
```
Now, the results are pulled back as a raw CSV text, but are immediately converted to a dataframe.

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/convert-raw-csv-to-dataframe.PNG)

## Run Inline Query in Looker API
One of the best features of the Looker API is the `run_inline_query()`. It allows the user to create queries which are converted into Looker queries which are similar to an "Explore" in the Looker user interface. 

These queries you define in code. E.g.,
```json
    "model": "demo_connection_model",
    "view": "base",                         
    "fields": [
        "clients.id",
        "static_demographics.veteran_text",
        "clients.added_date"
    ],
    "filters": {"clients.added_date": "after 2020-01-01"},
    "limit": 500
```
The above definition will produce a query similar to an Explore seen in the image below. 

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/run-inline-query-like-explore.png)

A few important bits of information which are needed to build queries successfully.

`view` actually refers to the "Explore" name.

| Explore Name         | View Name            |
| -------------------- | -------------------- |
| base                 | HMIS Performance     |
| client               | Coordinated Entry    |
| clients              | Services             |
| client_model         | Client Model         |
| data_quality         | Data Quality         |
| reservations_by_date | Reservations         |
| population           | Population Over Time |

Also, if you are looking for the field name which corresponds to a field name in Looker, the info bubble next to a dimension or measure name will contain the technical field name.

E.g, for the "Enrollment ID" it is `enrollments.id`.

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/looker-get-field-name.png)

Let's move on to some code!

```python
#############
# Setup
#############

# Load the Looker Python SDK tools.
import looker_sdk
from io import StringIO
import pandas as pd

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/path/to/your/looker.ini" # <--- Change 

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

#############
# Parameters
#############
RESULT_FORMAT = "csv"

#############
# Send Query
#############

# Define your query
body = {
    "model": "demo_connection_model",
    "view": "base",                         
    "fields": [
        "clients.id",
        "static_demographics.veteran_text",
        "clients.added_date"
    ],
    "filters": {"clients.added_date": "after 2020-01-01"},
    "limit": 100
}

################
# Get Response
################

# Run the inline query
result = sdk.run_inline_query(
    result_format=RESULT_FORMAT, 
    body=body
)

# Convert to dataframe.
df = pd.read_csv(StringIO(result))

```

If all goes well, your query will run and your results will be loaded into a dataframe.

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/looker-results-of-line-query.png)


## Retrieve Results from Looker Dashboard
Retrieving a dashboard from Looker API is a little bit trickier than pulling back a Look. But, still fairly easy. The trick is understanding dashboards are rendered. That is, they are data which have been converted into a mix of tables and visualizations and then saved in a common file format, such as `PDF`, `PNG`, or `JPG`.

The code below runs an existing dashboard, requests Looker render it as a `PDF`, then waits until Looker has finished. Once done, it is saved to a location defined in the script.

```python
#############
# Setup
#############
# Load the Looker Python SDK tools.
import looker_sdk
from time import sleep

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/path/to/your/looker.ini" # <--- Change 

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

#############
# Parameters
#############

# Where to save PDF.
PATH_TO_SAVE_OUTPUT = "C:/path/to/your/Desktop/Amazing_Dashboard.pdf"

# How many minutes to if render completed before timing out.
TIMEOUT_MINS = 15

# Dashboard ID. Found in the URL of the dashboard.
DASHBOARD_ID = 498

# Output type: pdf, png, or jpg
RESULT_FORMAT = "pdf"

BODY = {
    "dashboard_style": "tiled",
    # ADD FILTERS HERE
    # E.g.,  
    # "dashboard_filters": {
    #   "Created Date=12 months ago for 12 months"
    # }
}

# Output width in pixels
WIDTH = 640

# Output height in pixels
HEIGHT = 480

# Paper size for pdf. Value can be one of: ["letter","legal","tabloid","a0","a1","a2","a3","a4","a5"]
PDF_PAPER_SIZE = "letter"

# Whether to render pdf in landscape paper orientation
PDF_LANDSCAPE = True

#############
# Pull Data
#############

# Pull results from Look.
result = sdk.create_dashboard_render_task(
    dashboard_id = DASHBOARD_ID,
    body = BODY,
    result_format = RESULT_FORMAT,
    width = WIDTH,
    height = HEIGHT,
    pdf_paper_size = PDF_PAPER_SIZE,
    pdf_landscape = PDF_LANDSCAPE,                                          
)

task_id = result["id"]

#################################
# Wait for Dashboard to Render
#################################
# Below, we are going to check every minute
# if Looker has finished rendering your 
# dashboard. When it has, it saves it to
# as a file.

for minute in range(TIMEOUT_MINS):
    
    # Check if the dashboard has been rendered.
    task = sdk.render_task(task_id)["status"]
    print(f"Render is: {task}")
    
    if task == "success":
        
        # Get rendered results.
        result = sdk.render_task_results(task_id)
        
        # Write PDF.
        with open(PATH_TO_SAVE_OUTPUT, "wb+") as f:
            print(f"Dashboard saved to {PATH_TO_SAVE_OUTPUT}")
            f.write(result)

        # Quit early, if done.
        break
    
    else:
        print("Still not done, we'll check again in a minute.")
        sleep(60)
```

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/looker-api-rendered-pdf.PNG)


# Clarity DB in Python
Connecting directly to a database from within a Python script is extremely powerful. It allows enormous possibilities when automating data workflows. 

For example, let's say you have a PowerBI dashboard which must be updated daily. Unfortunately, there are many complex calculations which must be completed before pushing the results into PowerBI. Having access to your Clarity DB from within Python would allow you to create a script which retrieves the data from the Clarity DB, uses powerful Python data science tools for creating the calculations, and then save the results directly to PowerBI.

## Connecting to Clarity DB
There is a bit of setup work needed before accessing the Clarity SQL Databases through Python. 

* Connect to the VPN
* Create a connection file
* Install needed Python packages for MySQL / MariaDB

After purchasing Clarity SQL Access, you may request access credentials from the Clarity Help Desk. This will include instructions on connecting to the VPN.

After connecting to the VPN, let's create a database connection file. This will keep us from accidentally saving databases access credentials in one of our Python scripts.

To create a credential file, open notepad and enter the following:
```bash
[DEFAULT]
DB_NAME=YOUR_CLARITY_DB_NAME
DB_HOST=THE_IP_ADDRESS_OF_YOUR_DB
DB_PORT=3306
DB_USERNAME=YOUR_DB_USERNAME
DB_PASSWORD=YOUR_DB_PASSWORD
```

The information should have been provided the Clarity Help Desk team. If not, it may be requested. **Your DB credentials are essentially your username and password. Be extremely careful where you store them.** Please follow all the guidelines of your the policies from your IT and Security Administration teams.

Lastly, we need to install the Python packages which allow us to connect to MySQL / MariaDB. Open Anaconda Navigator and then "CMD.exe Prompt." There are several different ways to connect MySQL / MariaDB from Python, the following packages will ensure you can use most of them.

At the prompt, enter the following:
```bash
pip install -U pip
pip install mysqlclient
pip install mysql-connector-python
pip install pymysql
pip install SQLAlchemy
```

## Pulling Data from Clarity into a Dataframe
Now we have everything setup to write SQL queries in Python, let's take a look at how to query the Clarity SQL database, retrieve results, and convert them into a Python dataframe for further processing.

Run following code in your Python interpreter:
```python
###############
# Import Tools
###############
import configparser
from sqlalchemy import create_engine
import pandas as pd

###############
# Load DB Info.
###############
config = configparser.ConfigParser()
config.read("C:/Users/cthom/clarity_db.ini") # <-- Change 

###############
# Connect to DB
###############
db_connection = create_engine(
    "mysql+pymysql://" +\
    config["DEFAULT"]["DB_USERNAME"] + ":" +\
    config["DEFAULT"]["DB_PASSWORD"] + "@" +\
    config["DEFAULT"]["DB_HOST"] + "/" +\
    config["DEFAULT"]["DB_NAME"] 
)

##############################
# Read clients into Dataframe
##############################
sql_query = """
    SELECT c.id             AS personal_id,
           cp.id            AS enrollment_id,
           cp.start_date    AS start_date,
           cp.end_date      AS end_date,
           p.ref_category   AS project_type
      FROM clients AS c
      JOIN client_programs AS cp ON c.id = cp.ref_client
      JOIN programs AS p ON cp.ref_program = p.id
     WHERE cp.start_date > DATE_SUB(NOW(), INTERVAL 31 DAY)
"""

# Read from DB into dataframe.
df = pd.read_sql(sql_query, con=db_connection)
```
If all goes well, this should retrieve a list of clients who have had an enrollment in the last 30 days.

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/db-to-df.PNG)


## Combining Results from Clarity DB Query and Looker API
Probably the most powerful tool in the Clarity ad hoc reporting toolbox is combining data from the Clarity SQL Model and Looker API. This allows the query writer to write SQL queries to meet local reporting requirements set by county or municipalities, while leaning on the Clarity Looker data models for heavy HUD calculations such as Chronic Homelessness. 

The example below will hopefully exemplify the usefulness of the approach:

```python
###################
# Setup
###################

# Load the Looker Python SDK tools.
import looker_sdk
from io import StringIO
import configparser
from sqlalchemy import create_engine
import pandas as pd

###################
# Parameters
###################
RESULT_FORMAT = "csv"

###################
# Load DB Info.
###################
config = configparser.ConfigParser()
config.read("C:/path/to/your/db_creds.ini") # <-- Change 

###################
# Load Looker Info
###################

# Path to the `looker.ini` file we setup.
CREDENTIALS_PATH = "C:/path/to/your/looker.ini" # <--- Change 

# Initializing the Looker SDK.
sdk = looker_sdk.init40(CREDENTIALS_PATH)

###############
# Connect to DB
###############
db_connection = create_engine(
    "mysql+pymysql://" +\
    config["DEFAULT"]["DB_USERNAME"] + ":" +\
    config["DEFAULT"]["DB_PASSWORD"] + "@" +\
    config["DEFAULT"]["DB_HOST"] + "/" +\
    config["DEFAULT"]["DB_NAME"] 
)

##############################
# Query SQL database
##############################
sql_query = """
    SELECT c.id             AS personal_id,
           cp.id            AS enrollment_id,
           cp.start_date    AS start_date,
           cp.end_date      AS end_date,
           p.ref_category   AS project_type
      FROM clients AS c
      JOIN client_programs AS cp ON c.id = cp.ref_client
      JOIN programs AS p ON cp.ref_program = p.id
     WHERE cp.start_date > DATE_SUB(NOW(), INTERVAL 31 DAY)
"""

clarity_db_df = pd.read_sql(sql_query, con=db_connection)

##############################
# Get Chronic Homelesness
##############################

body = {
    "model": "demo_connection_model",
    "view": "base",                         
    "fields": [
        "enrollments.id",
        "chronic_homeless.ch_at_project_start"
    ],
    "limit": -1
}
 
looker_results = sdk.run_inline_query(
    result_format=RESULT_FORMAT, 
    body=body
)

chronic_homeless_df = pd.read_csv(StringIO(looker_results))

# Rename column names for ease of use.
chronic_homeless_df.columns = ["enrollment_id", "chronic"]

##############################
# Merge Results
##############################

# Join the two dataframes on enrollment_id
df = clarity_db_df.merge(
    chronic_homeless_df, 
    how="left",
    on="enrollment_id"    
)
```
The above script pulls results from the Clarity DB then pulls "Enrollment ID" and "Chronically Homeless at Project Start -- Individual." Lastly, these two dataframes are joined together.

![](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity/-/raw/master/images/combined-looker-and-db-results.png)

# Data Import Tool API
We've reviewed the different methods for programmatically exporting data from the Clarity system but there are times when importing data into Clarity is helpful for solving integration problems. This article will not cover general setup, as Bitfocus already has a document explaining how to setup the DIT API.

* [Clarity DIT API Credentials](https://get.clarityhs.help/hc/en-us/articles/115002555107-API-Credentials)

But the key to using the DIT API is understanding the HUD XML format, as the DIT API requires uploads to conform to the format.

Below is documentation regarding formatting XML to conform to HUD standards, which should ensure successful DIT API uploads.

* [HUD XML FY2024 Documentation](https://github.com/HUD-Data-Lab/xml/tree/FY2024-latest/src)

The code below grabs Clarity DIT credentials stored in a an `ini` file ([see example](https://gitlab.com/bitfocus-public/python-toolkit-for-clarity#setup-looker-api-credentials)). It then creates HUD XML for a client profile change and uploads it to Clarity.

```python
##############
# Setup
##############
import requests
import configparser
from datetime import datetime

config = configparser.ConfigParser()
config.read("C:/path/to/your/dit.ini") # <-- Change 

##############
# Parameters
##############

API_URL         = config["DEFAULT"]["API_URL"]
API_LOGIN       = config["DEFAULT"]["API_LOGIN"]
API_KEY         = config["DEFAULT"]["API_KEY"]

IS_MAP_DOC      = False

HUD_TIMESTAMP_FORMAT = "%Y-%m-%dT%H:%M:%S"

#############
# Setup
#############

url = API_URL + "map" if IS_MAP_DOC else API_URL + "data"

today = datetime.now().strftime(HUD_TIMESTAMP_FORMAT)

#############
# XML
#############

def xml_profile_builder(
    user_id,
    personal_id,
    first_name,
    middle_name,
    last_name,
    name_data_quality,
    ssn,
    ssn_data_quality,
    dob,
    dob_data_quality,
    race,
    ethnicity,
    gender,
    veteran_status,
):
    today = datetime.now().strftime(HUD_TIMESTAMP_FORMAT)
    return f"""
    <hmis:Client hmis:dateCreated="{today}" 
                 hmis:dateUpdated="{today}" 
                 hmis:userID="{user_id}"
    >
        <hmis:PersonalID>{personal_id}</hmis:PersonalID>
        <hmis:hmis:FirstName hmis:hashStatus="1">{first_name}</hmis:FirstName>
        <hmis:MiddleName hmis:hashStatus="1">{middle_name}</hmis:MiddleName>
        <hmis:LastName hmis:hashStatus="1">{last_name}</hmis:LastName>
        <hmis:NameDataQuality>{name_data_quality}</hmis:NameDataQuality>
        <hmis:SSN hmis:hashStatus="1">{ssn}</hmis:SSN>
        <hmis:SSNDataQuality>{ssn_data_quality}</hmis:SSNDataQuality>
        <hmis:DOB>{dob}</hmis:DOB>
        <hmis:DOBDataQuality>{dob_data_quality}</hmis:DOBDataQuality>
        <hmis:Race>{race}</hmis:Race>
        <hmis:Ethnicity>{ethnicity}</hmis:Ethnicity>
        <hmis:Gender>{gender}</hmis:Gender>
        <hmis:VeteranStatus>{veteran_status}</hmis:VeteranStatus>
    </hmis:Client>
    """

def hud_xml_builder(
    source_id,
    source_type,
    source_name,
    software_name,
    software_version,
    source_contact_email,
    source_contact_first_name,
    source_contact_last_name,
    export_id,
    export_date,
    export_start_date,
    export_end_date,
    profile
):
    return f"""
        <?xml version="1.0" encoding="UTF-8"?>
    <hmis:Sources xmlns:airs="http://www.clarityhumanservices.com/schema/2024/1/AIRS_3_0_mod.xsd"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:hmis="http://www.clarityhumanservices.com/schema/2024/1/HUD_HMIS.xsd">
        <hmis:Source>
            <hmis:SourceID>{source_id}</hmis:SourceID>
            <hmis:SourceType>{source_type}</hmis:SourceType>
            <hmis:SourceName>{source_name}</hmis:SourceName>
            <hmis:SoftwareName>{software_name}</hmis:SoftwareName>
            <hmis:SoftwareVersion>{software_version}</hmis:SoftwareVersion>
            <hmis:SourceContactEmail>{source_contact_email}</hmis:SourceContactEmail>
            <hmis:SourceContactFirst>{source_contact_first_name}</hmis:SourceContactFirst>
            <hmis:SourceContactLast>{source_contact_last_name}</hmis:SourceContactLast>
            <hmis:Export>
                <hmis:ExportID>{export_id}</hmis:ExportID>
                <hmis:ExportDate>{export_date}</hmis:ExportDate>
                <hmis:ExportPeriod>
                    <hmis:StartDate>{export_start_date}</hmis:StartDate>
                    <hmis:EndDate>{export_end_date}</hmis:EndDate>
                </hmis:ExportPeriod>
                <hmis:ExportPeriodType>reportingPeriod</hmis:ExportPeriodType>
                <hmis:ExportDirective>deltaRefresh</hmis:ExportDirective>   
                
                {profile}
            
            </hmis:Export>
        </hmis:Source>
    </hmis:Sources>
"""
#############
# Create Data
#############

profile = xml_profile_builder(
    user_id=1234,
    personal_id=1,
    first_name="Iven",
    middle_name="",
    last_name="Client",
    name_data_quality=1,
    ssn="123-45-6789",
    ssn_data_quality=1,
    dob="1984-09-01",
    dob_data_quality=1,
    race=5,
    ethnicity=0,
    gender=1,
    veteran_status=0,     
)

upload = hud_xml_builder(
    source_id=123,
    source_type=1,
    source_name="Test Upload",
    software_name="Bitfocus Test Integration",
    software_version="1.0",
    source_contact_email="test@email.com",
    source_contact_first_name="test_user_first_name",
    source_contact_last_name="test_user_last_name",
    export_id=12345, # Should be unique
    export_date=today,
    export_start_date=today,
    export_end_date=today,
    profile=profile
)

upload = """
<?xml version="1.0" encoding="UTF-8"?>
<hmis:Sources 
    xmlns:airs="http://www.clarityhumanservices.com/schema/2024/1/AIRS_3_0_mod.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:hmis="http://www.clarityhumanservices.com/schema/2024/1/HUD_HMIS.xsd">
  <hmis:Source>
    <hmis:SourceID>singleClientXml01</hmis:SourceID>
    <hmis:SourceType>1</hmis:SourceType>
    <hmis:SourceName>Single Client XML</hmis:SourceName>
    <hmis:SoftwareName>Bitfocus Demo Data</hmis:SoftwareName>
    <hmis:SoftwareVersion>1.0</hmis:SoftwareVersion>
    <hmis:SourceContactEmail>demo@bitfocus.com</hmis:SourceContactEmail>
    <hmis:SourceContactFirst>Demo</hmis:SourceContactFirst>
    <hmis:SourceContactLast>Import</hmis:SourceContactLast>
    <hmis:Export>
      <hmis:ExportID>singleClientXml01</hmis:ExportID>
      <hmis:ExportDate>2018-01-01T00:00:00</hmis:ExportDate>
      <hmis:ExportPeriod>
        <hmis:StartDate>2018-01-01T00:00:00</hmis:StartDate>
        <hmis:EndDate>2018-12-31T00:00:00</hmis:EndDate>
      </hmis:ExportPeriod>
      <hmis:ExportPeriodType>reportingPeriod</hmis:ExportPeriodType>
      <hmis:ExportDirective>deltaRefresh</hmis:ExportDirective>
      
      <!-- Standard Client Data -->
      <hmis:Client hmis:dateCreated="2018-01-01T00:00:00" hmis:dateUpdated="2018-01-01T00:00:00" hmis:userID="customFieldsUser01">
        <hmis:PersonalID>singleClient01</hmis:PersonalID>
        <hmis:FirstName hmis:hashStatus="1">Single</hmis:FirstName>
        <hmis:MiddleName hmis:hashStatus="1"></hmis:MiddleName>
        <hmis:LastName hmis:hashStatus="1">Client</hmis:LastName>
        <hmis:NameDataQuality>1</hmis:NameDataQuality>
        <hmis:SSN hmis:hashStatus="1">111111111</hmis:SSN>
        <hmis:SSNDataQuality>1</hmis:SSNDataQuality>
        <hmis:DOB>2000-01-01</hmis:DOB>
        <hmis:DOBDataQuality>1</hmis:DOBDataQuality>
        <hmis:Race>5</hmis:Race>
        <hmis:Ethnicity>0</hmis:Ethnicity>
        <hmis:Gender>1</hmis:Gender>
        <hmis:VeteranStatus>0</hmis:VeteranStatus>
      </hmis:Client>
      
    </hmis:Export>
  </hmis:Source>
</hmis:Sources>


"""

#############
# Upload
#############

# Add headers to notify API data type
headers = {
    "Content-Type":"text/xml",
    "Standard-Version": "2024",
    "Content-Encrypted": "0"    
}

import io
# Upload
r = requests.post(
    url,
    auth=(API_LOGIN, API_KEY),
    verify = True,
    headers=headers,
    files={'file': io.StringIO(upload)}
)

# Check Status
print(r.text)
```

# R
This is a bit out of place, but found there wasn't enough to create a separate article. Looker can also be used with the R language.

* [lookr](https://github.com/looker-open-source/lookr)
  
**Warning**: It appears interest in R has waned and Looker doesn't plan to continue supporting their `R` package:

* [Deprecation warning](https://github.com/looker-open-source/lookr/pull/39)

Though, the Looker web API is easy to work with, so using packages such as:
* [rjson](https://www.tutorialspoint.com/r/r_json_files.htm)
* [httr](https://cran.r-project.org/web/packages/httr/index.html)

Should a viable solution for working with Looker in the R ecosystem.

```r

# Install devtools
install.packages("devtools")
install.packages("httpuv")

# Install lookr
devtools::install_github("looker/lookr")

# Load lookr library.
library(lookr)

# Connect to Looker API.
sdk <- LookerSDK$new(configFile = "PATH_TO_YOUR_LOOKER_INI")

# Query a model
CONNECTION <- "YOUR_CONNECTION_MODEL_NAME"

# Explore name, for more info:
# https://gitlab.com/bitfocus-public/python-toolkit-for-clarity#run-inline-query-in-looker-api
VIEW = "base"

FIELDS <- c(
  "clients.id",
  "static_demographics.veteran_text",
  "clients.added_date"
)

results <- sdk$runInlineQuery(
  model = CONNECTION,
  view = VIEW,
  fields = FIELDS,
  limit = 500,
  queryTimezone = "America/Los_Angeles"
)

# Convert list of list to dataframe.
df <- as.data.frame(do.call(rbind, results))
```